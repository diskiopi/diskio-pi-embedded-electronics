EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MACE_LIB
LIBS:CB10LV102M
LIBS:LM3150MH
LIBS:carteReseau-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Carte Reseau"
Date "05/04/2018"
Rev "0.1"
Comp "Projet Diskio Pi - 0.3"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X08 P1
U 1 1 5AC4C5FD
P 2650 3600
F 0 "P1" H 2650 4050 50  0000 C CNN
F 1 "CONN_01X08" V 2750 3600 50  0000 C CNN
F 2 "diskioPi:JST-B8B-XH-A" H 2650 3600 50  0001 C CNN
F 3 "" H 2650 3600 50  0000 C CNN
F 4 "Value" H 2650 3600 60  0001 C CNN "Réf fab"
F 5 "Value" H 2650 3600 60  0001 C CNN "Code commande"
F 6 "Farnell" H 2650 3600 60  0001 C CNN "Distributeur"
F 7 "0.0" H 2650 3600 60  0001 C CNN "Prix"
	1    2650 3600
	-1   0    0    1   
$EndComp
Text Notes 2300 3050 0    79   ~ 0
JST B8B-XH-A
Wire Wire Line
	2850 3250 3100 3250
Wire Wire Line
	2850 3350 3100 3350
Wire Wire Line
	2850 3450 3100 3450
Wire Wire Line
	2850 3550 3100 3550
Wire Wire Line
	2850 3650 3100 3650
Wire Wire Line
	2850 3750 3100 3750
Wire Wire Line
	2850 3850 3100 3850
Wire Wire Line
	2850 3950 3100 3950
$Comp
L RJ45 J1
U 1 1 5AC5CD66
P 2800 5350
F 0 "J1" H 3000 5850 50  0000 C CNN
F 1 "RJ45" H 2650 5850 50  0000 C CNN
F 2 "diskioPi:RJSSE-5080" H 2800 5350 50  0001 C CNN
F 3 "" H 2800 5350 50  0000 C CNN
F 4 "Value" H 2800 5350 60  0001 C CNN "Réf fab"
F 5 "Value" H 2800 5350 60  0001 C CNN "Code commande"
F 6 "Farnell" H 2800 5350 60  0001 C CNN "Distributeur"
F 7 "0.0" H 2800 5350 60  0001 C CNN "Prix"
	1    2800 5350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 5700 3350 5700
Wire Wire Line
	3350 5600 3250 5600
Wire Wire Line
	3250 5500 3350 5500
Wire Wire Line
	3350 5400 3250 5400
Wire Wire Line
	3250 5300 3350 5300
Wire Wire Line
	3350 5200 3250 5200
Wire Wire Line
	3250 5100 3350 5100
Wire Wire Line
	3350 5000 3250 5000
Text GLabel 3350 5700 2    60   Input ~ 0
PIN1_RJ45
Text GLabel 3350 5600 2    60   Input ~ 0
PIN2_RJ45
Text GLabel 3350 5500 2    60   Input ~ 0
PIN3_RJ45
Text GLabel 3350 5400 2    60   Input ~ 0
PIN4_RJ45
Text GLabel 3350 5300 2    60   Input ~ 0
PIN5_RJ45
Text GLabel 3350 5200 2    60   Input ~ 0
PIN6_RJ45
Text GLabel 3350 5100 2    60   Input ~ 0
PIN7_RJ45
Text GLabel 3350 5000 2    60   Input ~ 0
PIN8_RJ45
Text GLabel 3100 3950 2    60   Input ~ 0
PIN1_RJ45
Text GLabel 3100 3850 2    60   Input ~ 0
PIN2_RJ45
Text GLabel 3100 3750 2    60   Input ~ 0
PIN3_RJ45
Text GLabel 3100 3650 2    60   Input ~ 0
PIN4_RJ45
Text GLabel 3100 3550 2    60   Input ~ 0
PIN5_RJ45
Text GLabel 3100 3450 2    60   Input ~ 0
PIN6_RJ45
Text GLabel 3100 3350 2    60   Input ~ 0
PIN7_RJ45
Text GLabel 3100 3250 2    60   Input ~ 0
PIN8_RJ45
Text Notes 2850 4750 0    79   ~ 0
Connecteur RJ45
$Comp
L Hole P3
U 1 1 5AC5D24D
P 5750 6250
F 0 "P3" H 5750 6350 50  0000 C CNN
F 1 "Hole" V 5850 6250 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5750 6250 60  0001 C CNN
F 3 "" H 5750 6250 60  0000 C CNN
F 4 "Value" H 5750 6250 60  0001 C CNN "Réf fab"
F 5 "Value" H 5750 6250 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5750 6250 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5750 6250 60  0001 C CNN "Prix"
	1    5750 6250
	-1   0    0    1   
$EndComp
$Comp
L Hole P5
U 1 1 5AC5D351
P 5750 6750
F 0 "P5" H 5750 6850 50  0000 C CNN
F 1 "Hole" V 5850 6750 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5750 6750 60  0001 C CNN
F 3 "" H 5750 6750 60  0000 C CNN
F 4 "Value" H 5750 6750 60  0001 C CNN "Réf fab"
F 5 "Value" H 5750 6750 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5750 6750 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5750 6750 60  0001 C CNN "Prix"
	1    5750 6750
	-1   0    0    1   
$EndComp
$Comp
L Hole P6
U 1 1 5AC5D376
P 5750 7000
F 0 "P6" H 5750 7100 50  0000 C CNN
F 1 "Hole" V 5850 7000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5750 7000 60  0001 C CNN
F 3 "" H 5750 7000 60  0000 C CNN
F 4 "Value" H 5750 7000 60  0001 C CNN "Réf fab"
F 5 "Value" H 5750 7000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5750 7000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5750 7000 60  0001 C CNN "Prix"
	1    5750 7000
	-1   0    0    1   
$EndComp
$Comp
L Hole P2
U 1 1 5AC5D3AA
P 5750 6000
F 0 "P2" H 5750 6100 50  0000 C CNN
F 1 "Hole" V 5850 6000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5750 6000 60  0001 C CNN
F 3 "" H 5750 6000 60  0000 C CNN
F 4 "Value" H 5750 6000 60  0001 C CNN "Réf fab"
F 5 "Value" H 5750 6000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5750 6000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5750 6000 60  0001 C CNN "Prix"
	1    5750 6000
	-1   0    0    1   
$EndComp
$Comp
L Hole P4
U 1 1 5AD23DAA
P 6200 7000
F 0 "P4" H 6200 7100 50  0000 C CNN
F 1 "Hole" V 6300 7000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 6200 7000 60  0001 C CNN
F 3 "" H 6200 7000 60  0000 C CNN
F 4 "Value" H 6200 7000 60  0001 C CNN "Réf fab"
F 5 "Value" H 6200 7000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6200 7000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6200 7000 60  0001 C CNN "Prix"
	1    6200 7000
	-1   0    0    1   
$EndComp
$EndSCHEMATC
