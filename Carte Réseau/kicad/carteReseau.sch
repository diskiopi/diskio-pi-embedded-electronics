EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Carte Reseau"
Date "05/04/2018"
Rev "0.1"
Comp "Projet Diskio Pi - 0.3"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L carteReseau-rescue:CONN_01X08-conn P1
U 1 1 5AC4C5FD
P 2650 3600
F 0 "P1" H 2650 4050 50  0000 C CNN
F 1 "CONN_01X08" V 2750 3600 50  0000 C CNN
F 2 "diskioPi:JST-B8B-XH-A" H 2650 3600 50  0001 C CNN
F 3 "" H 2650 3600 50  0000 C CNN
F 4 "B8B-XH-A (LF)(SN) " H 2650 3600 60  0001 C CNN "Réf fab"
F 5 "1516284" H 2650 3600 60  0001 C CNN "Code commande"
F 6 "Farnell" H 2650 3600 60  0001 C CNN "Distributeur"
F 7 "0,313" H 2650 3600 60  0001 C CNN "Prix"
	1    2650 3600
	-1   0    0    1   
$EndComp
Text Notes 2300 3050 0    79   ~ 0
JST B8B-XH-A
Wire Wire Line
	2850 3250 3100 3250
Wire Wire Line
	2850 3350 3100 3350
Wire Wire Line
	2850 3450 3100 3450
Wire Wire Line
	2850 3550 3100 3550
Wire Wire Line
	2850 3650 3100 3650
Wire Wire Line
	2850 3750 3100 3750
Wire Wire Line
	2850 3850 3100 3850
Wire Wire Line
	2850 3950 3100 3950
$Comp
L carteReseau-rescue:RJ45-conn J1
U 1 1 5AC5CD66
P 4900 3600
F 0 "J1" H 5100 4100 50  0000 C CNN
F 1 "RJ45" H 4750 4100 50  0000 C CNN
F 2 "diskioPi:RJSSE-5080" H 4900 3600 50  0001 C CNN
F 3 "" H 4900 3600 50  0000 C CNN
F 4 "RJSSE5080" H 4900 3600 60  0001 C CNN "Réf fab"
F 5 "2709044" H 4900 3600 60  0001 C CNN "Code commande"
F 6 "Farnell" H 4900 3600 60  0001 C CNN "Distributeur"
F 7 "1,12" H 4900 3600 60  0001 C CNN "Prix"
	1    4900 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 3950 5450 3950
Wire Wire Line
	5450 3850 5350 3850
Wire Wire Line
	5350 3750 5450 3750
Wire Wire Line
	5450 3650 5350 3650
Wire Wire Line
	5350 3550 5450 3550
Wire Wire Line
	5450 3450 5350 3450
Wire Wire Line
	5350 3350 5450 3350
Wire Wire Line
	5450 3250 5350 3250
Text GLabel 5450 3950 2    60   Input ~ 0
PIN1_RJ45
Text GLabel 5450 3850 2    60   Input ~ 0
PIN2_RJ45
Text GLabel 5450 3750 2    60   Input ~ 0
PIN3_RJ45
Text GLabel 5450 3650 2    60   Input ~ 0
PIN4_RJ45
Text GLabel 5450 3550 2    60   Input ~ 0
PIN5_RJ45
Text GLabel 5450 3450 2    60   Input ~ 0
PIN6_RJ45
Text GLabel 5450 3350 2    60   Input ~ 0
PIN7_RJ45
Text GLabel 5450 3250 2    60   Input ~ 0
PIN8_RJ45
Text GLabel 3100 3950 2    60   Input ~ 0
PIN1_RJ45
Text GLabel 3100 3850 2    60   Input ~ 0
PIN2_RJ45
Text GLabel 3100 3750 2    60   Input ~ 0
PIN3_RJ45
Text GLabel 3100 3650 2    60   Input ~ 0
PIN4_RJ45
Text GLabel 3100 3550 2    60   Input ~ 0
PIN5_RJ45
Text GLabel 3100 3450 2    60   Input ~ 0
PIN6_RJ45
Text GLabel 3100 3350 2    60   Input ~ 0
PIN7_RJ45
Text GLabel 3100 3250 2    60   Input ~ 0
PIN8_RJ45
Text Notes 4950 3000 0    79   ~ 0
Connecteur RJ45
$Comp
L carteReseau-rescue:Hole-MACE_LIB P3
U 1 1 5AC5D24D
P 5000 7000
F 0 "P3" H 5000 7100 50  0000 C CNN
F 1 "Hole" V 5100 7000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5000 7000 60  0001 C CNN
F 3 "" H 5000 7000 60  0000 C CNN
F 4 "Value" H 5000 7000 60  0001 C CNN "Réf fab"
F 5 "Value" H 5000 7000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5000 7000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5000 7000 60  0001 C CNN "Prix"
	1    5000 7000
	-1   0    0    1   
$EndComp
$Comp
L carteReseau-rescue:Hole-MACE_LIB P5
U 1 1 5AC5D351
P 5750 6750
F 0 "P5" H 5750 6850 50  0000 C CNN
F 1 "Hole" V 5850 6750 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5750 6750 60  0001 C CNN
F 3 "" H 5750 6750 60  0000 C CNN
F 4 "Value" H 5750 6750 60  0001 C CNN "Réf fab"
F 5 "Value" H 5750 6750 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5750 6750 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5750 6750 60  0001 C CNN "Prix"
	1    5750 6750
	-1   0    0    1   
$EndComp
$Comp
L carteReseau-rescue:Hole-MACE_LIB P6
U 1 1 5AC5D376
P 5750 7000
F 0 "P6" H 5750 7100 50  0000 C CNN
F 1 "Hole" V 5850 7000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5750 7000 60  0001 C CNN
F 3 "" H 5750 7000 60  0000 C CNN
F 4 "Value" H 5750 7000 60  0001 C CNN "Réf fab"
F 5 "Value" H 5750 7000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5750 7000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5750 7000 60  0001 C CNN "Prix"
	1    5750 7000
	-1   0    0    1   
$EndComp
$Comp
L carteReseau-rescue:Hole-MACE_LIB P2
U 1 1 5AC5D3AA
P 5000 6750
F 0 "P2" H 5000 6850 50  0000 C CNN
F 1 "Hole" V 5100 6750 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5000 6750 60  0001 C CNN
F 3 "" H 5000 6750 60  0000 C CNN
F 4 "Value" H 5000 6750 60  0001 C CNN "Réf fab"
F 5 "Value" H 5000 6750 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5000 6750 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5000 6750 60  0001 C CNN "Prix"
	1    5000 6750
	-1   0    0    1   
$EndComp
$Comp
L carteReseau-rescue:Hole-MACE_LIB P4
U 1 1 5AD23DAA
P 6200 7000
F 0 "P4" H 6200 7100 50  0000 C CNN
F 1 "Hole" V 6300 7000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 6200 7000 60  0001 C CNN
F 3 "" H 6200 7000 60  0000 C CNN
F 4 "Value" H 6200 7000 60  0001 C CNN "Réf fab"
F 5 "Value" H 6200 7000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6200 7000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6200 7000 60  0001 C CNN "Prix"
	1    6200 7000
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x05 J3
U 1 1 5D205574
P 5250 2250
F 0 "J3" H 5330 2292 50  0000 L CNN
F 1 "B5B-XH-A (LF)(SN) " H 5330 2201 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B5B-XH-A_1x05_P2.50mm_Vertical" H 5250 2250 50  0001 C CNN
F 3 "~" H 5250 2250 50  0001 C CNN
F 4 "B5B-XH-A (LF)(SN) " H 0   0   50  0001 C CNN "Réf fab"
F 5 "1516281" H 0   0   50  0001 C CNN "Code commande"
F 6 "Farnell" H 0   0   50  0001 C CNN "Distributeur"
F 7 "0,151" H 0   0   50  0001 C CNN "Prix"
	1    5250 2250
	1    0    0    -1  
$EndComp
Text GLabel 5000 2450 0    39   Input ~ 0
AUDIO_1
Wire Wire Line
	5050 2450 5000 2450
Wire Wire Line
	5050 2350 5000 2350
Wire Wire Line
	5000 2250 5050 2250
Wire Wire Line
	5050 2150 5000 2150
Wire Wire Line
	5000 2050 5050 2050
Text GLabel 5000 2350 0    39   Input ~ 0
AUDIO_2
Text GLabel 5000 2250 0    39   Input ~ 0
AUDIO_3
Text GLabel 5000 2150 0    39   Input ~ 0
AUDIO_4
Text GLabel 5000 2050 0    39   Input ~ 0
AUDIO_5
Text GLabel 4850 5100 0    60   Input ~ 0
SD_DAT2
Wire Wire Line
	6100 4900 5950 4900
Text GLabel 4850 5200 0    60   Input ~ 0
SD_DAT3
Text GLabel 4850 5300 0    60   Input ~ 0
SD_CMD
Text GLabel 4850 4800 0    60   Input ~ 0
SD_3.3V
Text GLabel 4850 5400 0    60   Input ~ 0
SD_CLK
Text GLabel 4850 5500 0    60   Input ~ 0
SD_GND
Text GLabel 4850 4900 0    60   Input ~ 0
SD_DAT0
Text GLabel 4850 5000 0    60   Input ~ 0
SD_DAT1
$Comp
L MEM2051:MEM2051-00-195-00-A_REVC J4
U 1 1 5FFB816D
P 6300 5300
F 0 "J4" H 6930 5346 50  0000 L CNN
F 1 "MEM2051-00-195-00-A_REVC" H 6930 5255 50  0000 L CNN
F 2 "MEM2051:GCT_MEM2051-00-195-00-A_REVC" H 6300 5300 50  0001 L BNN
F 3 "" H 6300 5300 50  0001 L BNN
F 4 "GCT" H 6300 5300 50  0001 L BNN "MANUFACTURER"
	1    6300 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5700 5900 5700
Wire Wire Line
	6100 5800 5900 5800
NoConn ~ 5900 5700
$Comp
L MOLEX_505110:MOLEX_200528-0150 006207341915000+1
U 1 1 5FFEE94B
P 2650 6050
F 0 "006207341915000+1" H 2322 6697 60  0000 R CNN
F 1 "006207341915000+" H 2322 6803 60  0000 R CNN
F 2 "Connectors:006207341915000+" H 2600 7850 60  0001 C CNN
F 3 "" H 2600 7850 60  0001 C CNN
	1    2650 6050
	1    0    0    -1  
$EndComp
Text GLabel 3150 6050 2    60   Input ~ 0
SD_GND
Text GLabel 3150 5850 2    60   Input ~ 0
SD_GND
Text GLabel 3150 5650 2    60   Input ~ 0
SD_GND
Text GLabel 3150 5450 2    60   Input ~ 0
SD_GND
Text GLabel 3150 5250 2    60   Input ~ 0
SD_GND
Text GLabel 3150 4850 2    60   Input ~ 0
SD_GND
Text GLabel 3150 4650 2    60   Input ~ 0
SD_GND
Text GLabel 3150 5750 2    60   Input ~ 0
SD_DAT3
Text GLabel 3150 5950 2    60   Input ~ 0
SD_DAT2
Text GLabel 3150 5050 2    60   Input ~ 0
SD_GND
Wire Wire Line
	3150 4850 2850 4850
Wire Wire Line
	3150 4950 2850 4950
Wire Wire Line
	3150 4650 2850 4650
Wire Wire Line
	3150 4750 2850 4750
Wire Wire Line
	2850 5050 3150 5050
Wire Wire Line
	3150 5150 2850 5150
Wire Wire Line
	2850 5250 3150 5250
Wire Wire Line
	3150 5350 2850 5350
Wire Wire Line
	2850 5450 3150 5450
Wire Wire Line
	3150 5550 2850 5550
Wire Wire Line
	2850 5650 3150 5650
Wire Wire Line
	3150 5750 2850 5750
Wire Wire Line
	2850 5850 3150 5850
Wire Wire Line
	3150 5950 2850 5950
Wire Wire Line
	2850 6050 3150 6050
Text GLabel 3150 4750 2    60   Input ~ 0
SD_DAT1
Text GLabel 3150 4950 2    60   Input ~ 0
SD_DAT0
Text GLabel 3150 5150 2    60   Input ~ 0
SD_CLK
Text GLabel 3150 5350 2    60   Input ~ 0
SD_3.3V
Text GLabel 3150 5550 2    60   Input ~ 0
SD_CMD
$Comp
L audio:SJ2-35564A-SMT-TR J2
U 1 1 6000AB9D
P 2800 2100
F 0 "J2" H 2907 2867 50  0000 C CNN
F 1 "SJ2-35564A-SMT-TR" H 2907 2776 50  0000 C CNN
F 2 "carte-lan:CUI_SJ2-35564A-SMT-TR" H 2800 2100 50  0001 L BNN
F 3 "" H 2800 2100 50  0001 L BNN
F 4 "SJ2-35564A-SMT-TR" H 2800 2100 50  0001 L BNN "MPN"
F 5 "CUI" H 2800 2100 50  0001 L BNN "MF"
F 6 "1.0" H 2800 2100 50  0001 L BNN "PART_REV"
F 7 "Manufacturer recommendation" H 2800 2100 50  0001 L BNN "STANDARD"
	1    2800 2100
	1    0    0    -1  
$EndComp
Text GLabel 3500 1600 2    39   Input ~ 0
AUDIO_1
Wire Wire Line
	3400 1600 3500 1600
Wire Wire Line
	3500 2400 3400 2400
Wire Wire Line
	3400 2000 3500 2000
Wire Wire Line
	3400 1800 3500 1800
Text GLabel 3500 2000 2    39   Input ~ 0
AUDIO_4
Text GLabel 3500 1800 2    39   Input ~ 0
AUDIO_2
Text GLabel 3500 2400 2    39   Input ~ 0
AUDIO_3
Text GLabel 3500 2600 2    39   Input ~ 0
AUDIO_5
Wire Wire Line
	3400 2600 3500 2600
Text GLabel 4850 5950 0    60   Input ~ 0
SD_GND
$Comp
L Device:R R3
U 1 1 6020BE14
P 5550 4500
F 0 "R3" V 5550 4750 50  0000 C CNN
F 1 "27k" V 5550 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5480 4500 50  0001 C CNN
F 3 "~" H 5550 4500 50  0001 C CNN
	1    5550 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 6020C33B
P 5750 4500
F 0 "R4" V 5750 4750 50  0000 C CNN
F 1 "27k" V 5750 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5680 4500 50  0001 C CNN
F 3 "~" H 5750 4500 50  0001 C CNN
	1    5750 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6020C750
P 5350 4500
F 0 "R2" V 5350 4750 50  0000 C CNN
F 1 "27k" V 5350 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5280 4500 50  0001 C CNN
F 3 "~" H 5350 4500 50  0001 C CNN
	1    5350 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 6020D288
P 5950 4500
F 0 "R5" V 5950 4750 50  0000 C CNN
F 1 "27k" V 5950 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5880 4500 50  0001 C CNN
F 3 "~" H 5950 4500 50  0001 C CNN
	1    5950 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4800 6100 4350
Wire Wire Line
	6100 4350 5950 4350
Wire Wire Line
	5950 4350 5750 4350
Connection ~ 5950 4350
Wire Wire Line
	5750 4350 5550 4350
Connection ~ 5750 4350
Wire Wire Line
	5550 4350 5350 4350
Connection ~ 5550 4350
Wire Wire Line
	4900 4350 4900 4800
Wire Wire Line
	4900 4800 4850 4800
Connection ~ 5350 4350
Wire Wire Line
	5950 4650 5950 4900
Connection ~ 5950 4900
Wire Wire Line
	5750 4650 5750 5000
Wire Wire Line
	5750 5000 6100 5000
Connection ~ 5750 5000
Wire Wire Line
	5550 4650 5550 5100
Wire Wire Line
	5550 5100 6100 5100
Connection ~ 5550 5100
Wire Wire Line
	5350 4650 5350 5200
Wire Wire Line
	5350 5200 6100 5200
Connection ~ 5350 5200
$Comp
L Device:R R1
U 1 1 60243D23
P 5150 4500
F 0 "R1" V 5150 4750 50  0000 C CNN
F 1 "27k" V 5150 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5080 4500 50  0001 C CNN
F 3 "~" H 5150 4500 50  0001 C CNN
	1    5150 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5300 5150 4650
Wire Wire Line
	5150 5300 6100 5300
Wire Wire Line
	5150 5300 4850 5300
Connection ~ 5150 5300
Wire Wire Line
	4850 5200 5350 5200
Wire Wire Line
	4850 5100 5550 5100
Wire Wire Line
	4850 5000 5750 5000
Wire Wire Line
	4850 4900 5950 4900
Wire Wire Line
	4900 4350 5150 4350
Wire Wire Line
	5150 4350 5350 4350
Connection ~ 5150 4350
$Comp
L Device:C C1
U 1 1 6025A275
P 4900 5800
F 0 "C1" H 5015 5846 50  0000 L CNN
F 1 "0.1uF" H 5015 5755 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4938 5650 50  0001 C CNN
F 3 "~" H 4900 5800 50  0001 C CNN
	1    4900 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 5950 4900 5950
Wire Wire Line
	5900 5950 5900 5800
Wire Wire Line
	4900 5650 4900 4800
Connection ~ 4900 4800
Wire Wire Line
	4850 5400 6100 5400
Wire Wire Line
	4850 5500 6100 5500
Connection ~ 4900 5950
Wire Wire Line
	4900 5950 5900 5950
$EndSCHEMATC
