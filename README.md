# Diskio Pi embedded electronics

Fichiers GERBER et circuits permettant de fabriquer les cartes qui font fonctionner le terminal Diskio Pi.

GERBER and circuits to make the boards that run Diskio Pi terminal.

![a preview of the daughter board](PCB-carte-fille.png)

![a preview of the charger board](PCB-carte-chargeur.png)

## Récupérer les fichiers

```
git clone https://gitlab.com/diskiopi/diskio-pi-embedded-electronics.git
```

## Prérequis

La modification des fichiers peut être effectuée grâce à des logiciels tel que Kicad.

The modification of the files can be done using software such as Kicad.

## Contribuer au projet

Veuillez lire le fichier [CONTRIBUTION.md](https://gitlab.com/diskiopi/diskio-pi-embedded-electronics/blob/master/CONTRIBUTION.md) pour connaître les détails de notre code de conduite et le processus de demande pour obtenir un 'pull'

## Versions actuelles

Carte fille / Carte chargeur: 1.2

Daughter board / Charger board: 1.2

Carte SATA: 0.1

SATA board: 0.1

## Wiki

Les instructions de montage du kit sont disponibles ici : [Wiki Diskio Pi](https://www.diskiopi.com/wiki/)

Mounting instructions for the kit are available here : [Wiki Diskio Pi](https://www.diskiopi.com/wiki/)

## Auteurs

* **Nicolas MACE** - *Travail initial* - [Nicolas](http://fr.macerobotics.com/)
* **Guillaume DEBRAY** - *Travail initial* - [Guillaume](https://gitlab.com/DiskioPi)

## Licence

Ce projet est enregistré sous licence OSHW sous le numéro [FR000007](https://certification.oshwa.org/fr000007.html)

