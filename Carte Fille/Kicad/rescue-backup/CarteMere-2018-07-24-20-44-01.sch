EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MACE_LIB
LIBS:CarteRack-cache
LIBS:CB10LV102M
LIBS:LM3150MH
LIBS:+9v
LIBS:+12v
LIBS:4p04l03
LIBS:carte_mere-cache
LIBS:carte_mere-rescue
LIBS:gl850g-lqfp-48
LIBS:gl850g-qfn-28
LIBS:gl850g-ssop-28
LIBS:hub_usb2-rescue
LIBS:molex-505110-1992
LIBS:prtr5v0u4d,125
LIBS:CarteMere-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title "Carte mere Diskio Pi"
Date "09/05/2018"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2850 2850 1400 750 
U 5AC62C5C
F0 "StepDown" 60
F1 "StepDown.sch" 60
$EndSheet
$Sheet
S 2850 3850 1400 600 
U 5AC62C5F
F0 "HUB_USB" 60
F1 "HUB_USB.sch" 60
$EndSheet
$Sheet
S 2850 4700 1400 500 
U 5AC62D94
F0 "Ventilo" 60
F1 "Ventilo.sch" 60
$EndSheet
$Sheet
S 4550 4700 1150 500 
U 5AC62DB0
F0 "InterrupteurPower" 60
F1 "InterrupteurPower.sch" 60
$EndSheet
$Sheet
S 4550 3900 1150 500 
U 5AC62DCE
F0 "Timer" 60
F1 "Timer.sch" 60
$EndSheet
$Comp
L Hole P1
U 1 1 5AC630E6
P 4100 6000
F 0 "P1" H 4100 6100 50  0000 C CNN
F 1 "Hole" V 4200 6000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 4100 6000 60  0001 C CNN
F 3 "" H 4100 6000 60  0000 C CNN
F 4 "Value" H 4100 6000 60  0001 C CNN "Réf fab"
F 5 "Value" H 4100 6000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 4100 6000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 4100 6000 60  0001 C CNN "Prix"
	1    4100 6000
	1    0    0    -1  
$EndComp
$Comp
L Hole P2
U 1 1 5AC63227
P 4100 6250
F 0 "P2" H 4100 6350 50  0000 C CNN
F 1 "Hole" V 4200 6250 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 4100 6250 60  0001 C CNN
F 3 "" H 4100 6250 60  0000 C CNN
F 4 "Value" H 4100 6250 60  0001 C CNN "Réf fab"
F 5 "Value" H 4100 6250 60  0001 C CNN "Code commande"
F 6 "Farnell" H 4100 6250 60  0001 C CNN "Distributeur"
F 7 "0.0" H 4100 6250 60  0001 C CNN "Prix"
	1    4100 6250
	1    0    0    -1  
$EndComp
$Comp
L Hole P3
U 1 1 5AC6324B
P 4100 6500
F 0 "P3" H 4100 6600 50  0000 C CNN
F 1 "Hole" V 4200 6500 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 4100 6500 60  0001 C CNN
F 3 "" H 4100 6500 60  0000 C CNN
F 4 "Value" H 4100 6500 60  0001 C CNN "Réf fab"
F 5 "Value" H 4100 6500 60  0001 C CNN "Code commande"
F 6 "Farnell" H 4100 6500 60  0001 C CNN "Distributeur"
F 7 "0.0" H 4100 6500 60  0001 C CNN "Prix"
	1    4100 6500
	1    0    0    -1  
$EndComp
$Comp
L Hole P4
U 1 1 5AC63272
P 4100 6750
F 0 "P4" H 4100 6850 50  0000 C CNN
F 1 "Hole" V 4200 6750 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 4100 6750 60  0001 C CNN
F 3 "" H 4100 6750 60  0000 C CNN
F 4 "Value" H 4100 6750 60  0001 C CNN "Réf fab"
F 5 "Value" H 4100 6750 60  0001 C CNN "Code commande"
F 6 "Farnell" H 4100 6750 60  0001 C CNN "Distributeur"
F 7 "0.0" H 4100 6750 60  0001 C CNN "Prix"
	1    4100 6750
	1    0    0    -1  
$EndComp
$Comp
L Hole P5
U 1 1 5AC6338C
P 5000 6000
F 0 "P5" H 5000 6100 50  0000 C CNN
F 1 "Hole" V 5100 6000 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5000 6000 60  0001 C CNN
F 3 "" H 5000 6000 60  0000 C CNN
F 4 "Value" H 5000 6000 60  0001 C CNN "Réf fab"
F 5 "Value" H 5000 6000 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5000 6000 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5000 6000 60  0001 C CNN "Prix"
	1    5000 6000
	1    0    0    -1  
$EndComp
$Comp
L Hole P6
U 1 1 5AC63396
P 5000 6250
F 0 "P6" H 5000 6350 50  0000 C CNN
F 1 "Hole" V 5100 6250 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5000 6250 60  0001 C CNN
F 3 "" H 5000 6250 60  0000 C CNN
F 4 "Value" H 5000 6250 60  0001 C CNN "Réf fab"
F 5 "Value" H 5000 6250 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5000 6250 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5000 6250 60  0001 C CNN "Prix"
	1    5000 6250
	1    0    0    -1  
$EndComp
$Comp
L Hole P7
U 1 1 5AC633A0
P 5000 6500
F 0 "P7" H 5000 6600 50  0000 C CNN
F 1 "Hole" V 5100 6500 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5000 6500 60  0001 C CNN
F 3 "" H 5000 6500 60  0000 C CNN
F 4 "Value" H 5000 6500 60  0001 C CNN "Réf fab"
F 5 "Value" H 5000 6500 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5000 6500 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5000 6500 60  0001 C CNN "Prix"
	1    5000 6500
	1    0    0    -1  
$EndComp
$Comp
L Hole P8
U 1 1 5AC633AA
P 5000 6750
F 0 "P8" H 5000 6850 50  0000 C CNN
F 1 "Hole" V 5100 6750 50  0000 C CNN
F 2 "lib_robot:Hole_4mm" H 5000 6750 60  0001 C CNN
F 3 "" H 5000 6750 60  0000 C CNN
F 4 "Value" H 5000 6750 60  0001 C CNN "Réf fab"
F 5 "Value" H 5000 6750 60  0001 C CNN "Code commande"
F 6 "Farnell" H 5000 6750 60  0001 C CNN "Distributeur"
F 7 "0.0" H 5000 6750 60  0001 C CNN "Prix"
	1    5000 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 6000 3850 6000
Wire Wire Line
	3850 6000 3850 6900
Wire Wire Line
	3850 6750 3900 6750
Wire Wire Line
	3900 6500 3850 6500
Connection ~ 3850 6500
Wire Wire Line
	3900 6250 3850 6250
Connection ~ 3850 6250
Wire Wire Line
	4800 6000 4750 6000
Wire Wire Line
	4750 6000 4750 6900
Wire Wire Line
	4750 6750 4800 6750
Wire Wire Line
	4800 6500 4750 6500
Connection ~ 4750 6500
Wire Wire Line
	4800 6250 4750 6250
Connection ~ 4750 6250
Connection ~ 4750 6750
Connection ~ 3850 6750
Wire Wire Line
	4750 6900 3850 6900
$Comp
L GND #PWR01
U 1 1 5AC6347A
P 3850 6900
F 0 "#PWR01" H 3850 6650 50  0001 C CNN
F 1 "GND" H 3850 6750 50  0000 C CNN
F 2 "" H 3850 6900 50  0000 C CNN
F 3 "" H 3850 6900 50  0000 C CNN
	1    3850 6900
	1    0    0    -1  
$EndComp
Connection ~ 3850 6900
Text Notes 5550 1900 0    118  ~ 0
Une led pour le step down\nUne led pour l'écran\nUne led pour le RPi
$Sheet
S 5900 4700 1350 500 
U 5AD8C780
F0 "HDMI" 118
F1 "HDMI.sch" 118
$EndSheet
Connection ~ -900 8000
$Comp
L FIDUCIAL FID1
U 1 1 5B2D55F7
P 6050 5850
F 0 "FID1" H 6050 6000 50  0000 C CNN
F 1 "FIDUCIAL" H 6050 5700 50  0000 C CNN
F 2 "lib_robot:FIDUCIAL" H 6050 5850 60  0001 C CNN
F 3 "" H 6050 5850 60  0000 C CNN
F 4 "Value" H 6050 5850 60  0001 C CNN "Réf fab"
F 5 "Value" H 6050 5850 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6050 5850 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6050 5850 60  0001 C CNN "Prix"
	1    6050 5850
	1    0    0    -1  
$EndComp
$Comp
L FIDUCIAL FID3
U 1 1 5B2D56E0
P 6500 5850
F 0 "FID3" H 6500 6000 50  0000 C CNN
F 1 "FIDUCIAL" H 6500 5700 50  0000 C CNN
F 2 "lib_robot:FIDUCIAL" H 6500 5850 60  0001 C CNN
F 3 "" H 6500 5850 60  0000 C CNN
F 4 "Value" H 6500 5850 60  0001 C CNN "Réf fab"
F 5 "Value" H 6500 5850 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6500 5850 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6500 5850 60  0001 C CNN "Prix"
	1    6500 5850
	1    0    0    -1  
$EndComp
$Comp
L FIDUCIAL FID2
U 1 1 5B2D572A
P 6050 6300
F 0 "FID2" H 6050 6450 50  0000 C CNN
F 1 "FIDUCIAL" H 6050 6150 50  0000 C CNN
F 2 "lib_robot:FIDUCIAL" H 6050 6300 60  0001 C CNN
F 3 "" H 6050 6300 60  0000 C CNN
F 4 "Value" H 6050 6300 60  0001 C CNN "Réf fab"
F 5 "Value" H 6050 6300 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6050 6300 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6050 6300 60  0001 C CNN "Prix"
	1    6050 6300
	1    0    0    -1  
$EndComp
$Comp
L FIDUCIAL FID4
U 1 1 5B2D5777
P 6500 6300
F 0 "FID4" H 6500 6450 50  0000 C CNN
F 1 "FIDUCIAL" H 6500 6150 50  0000 C CNN
F 2 "lib_robot:FIDUCIAL" H 6500 6300 60  0001 C CNN
F 3 "" H 6500 6300 60  0000 C CNN
F 4 "Value" H 6500 6300 60  0001 C CNN "Réf fab"
F 5 "Value" H 6500 6300 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6500 6300 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6500 6300 60  0001 C CNN "Prix"
	1    6500 6300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
