EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MACE_LIB
LIBS:CarteBouton-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Carte Bouton"
Date "05/04/2018"
Rev "0.1"
Comp "Projet Diskio Pi - 0.3"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X04 P3
U 1 1 5AC4BF6F
P 3550 3250
F 0 "P3" H 3550 3500 50  0000 C CNN
F 1 "CONN_01X04" V 3650 3250 50  0000 C CNN
F 2 "21_DiskioPi-03:SM04B-SRSS-TB(LF)(SN)" H 3550 3250 50  0001 C CNN
F 3 "" H 3550 3250 50  0000 C CNN
F 4 "SM04B-SRSS-TB(LF)(SN" H 3550 3250 60  0001 C CNN "Réf fab"
F 5 "1830839" H 3550 3250 60  0001 C CNN "Code commande"
F 6 "Farnell" H 3550 3250 60  0001 C CNN "Distributeur"
F 7 "0.0" H 3550 3250 60  0001 C CNN "Prix"
	1    3550 3250
	-1   0    0    1   
$EndComp
Text Notes 3050 2850 0    79   ~ 0
Connecteur JST\nSM04B-SRSS-TB
$Comp
L Hole P1
U 1 1 5AC4C13A
P 3500 4050
F 0 "P1" H 3500 4150 50  0000 C CNN
F 1 "Hole" V 3600 4050 50  0000 C CNN
F 2 "lib_robot:Hole_3mm" H 3500 4050 60  0001 C CNN
F 3 "" H 3500 4050 60  0000 C CNN
F 4 "Value" H 3500 4050 60  0001 C CNN "Réf fab"
F 5 "Value" H 3500 4050 60  0001 C CNN "Code commande"
F 6 "Farnell" H 3500 4050 60  0001 C CNN "Distributeur"
F 7 "0.0" H 3500 4050 60  0001 C CNN "Prix"
	1    3500 4050
	-1   0    0    1   
$EndComp
$Comp
L Hole P2
U 1 1 5AC4C306
P 3500 4300
F 0 "P2" H 3500 4400 50  0000 C CNN
F 1 "Hole" V 3600 4300 50  0000 C CNN
F 2 "lib_robot:Hole_3mm" H 3500 4300 60  0001 C CNN
F 3 "" H 3500 4300 60  0000 C CNN
F 4 "Value" H 3500 4300 60  0001 C CNN "Réf fab"
F 5 "Value" H 3500 4300 60  0001 C CNN "Code commande"
F 6 "Farnell" H 3500 4300 60  0001 C CNN "Distributeur"
F 7 "0.0" H 3500 4300 60  0001 C CNN "Prix"
	1    3500 4300
	-1   0    0    1   
$EndComp
$Comp
L SS314MAH4 P4
U 1 1 5ACDE053
P 6850 3250
F 0 "P4" H 6850 3500 50  0000 C CNN
F 1 "SS314MAH4" V 6950 3250 50  0000 C CNN
F 2 "diskioPi:SS314MAH4" H 6850 3250 50  0001 C CNN
F 3 "" H 6850 3250 50  0000 C CNN
F 4 "Value" H 6850 3250 60  0001 C CNN "Réf fab"
F 5 "Value" H 6850 3250 60  0001 C CNN "Code commande"
F 6 "Farnell" H 6850 3250 60  0001 C CNN "Distributeur"
F 7 "0.0" H 6850 3250 60  0001 C CNN "Prix"
	1    6850 3250
	1    0    0    -1  
$EndComp
$Comp
L +9V #PWR02
U 1 1 5AD5FC3F
P 4800 3200
F 0 "#PWR02" H 4800 3050 50  0001 C CNN
F 1 "+9V" H 4800 3340 50  0000 C CNN
F 2 "" H 4800 3200 50  0000 C CNN
F 3 "" H 4800 3200 50  0000 C CNN
	1    4800 3200
	1    0    0    -1  
$EndComp
$Comp
L +9V #PWR03
U 1 1 5AD5FDB6
P 5600 3200
F 0 "#PWR03" H 5600 3050 50  0001 C CNN
F 1 "+9V" H 5600 3340 50  0000 C CNN
F 2 "" H 5600 3200 50  0000 C CNN
F 3 "" H 5600 3200 50  0000 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AD5FF48
P 3800 3450
F 0 "#PWR04" H 3800 3200 50  0001 C CNN
F 1 "GND" H 3800 3300 50  0000 C CNN
F 2 "" H 3800 3450 50  0000 C CNN
F 3 "" H 3800 3450 50  0000 C CNN
	1    3800 3450
	1    0    0    -1  
$EndComp
Text GLabel 3850 3100 2    39   Input ~ 0
U_POSITION1
Text Notes 8850 2300 0    118  ~ 0
Postion 1 : \n- ON : écran\n- ON : Raspberry Pi\n\nPostion 2 : OFF\n\nPostion 3 : \n- ON : écran\n- OFF : Raspberry Pi\n
Text Notes 6150 2650 0    118  ~ 0
Switch 3 positions
Text Notes 7050 3150 0    39   ~ 0
position 1
Text Notes 7050 3300 0    39   ~ 0
position 2 (off)
Text Notes 7050 3500 0    39   ~ 0
position 3
Text GLabel 3850 3200 2    39   Input ~ 0
U_POSITION3
Wire Wire Line
	3750 3100 3850 3100
Wire Wire Line
	3850 3200 3750 3200
Wire Wire Line
	4800 3300 4800 3200
Wire Wire Line
	3750 3300 4800 3300
Wire Wire Line
	3800 3400 3750 3400
Wire Wire Line
	3800 3450 3800 3400
Wire Wire Line
	5600 3300 5600 3200
Wire Wire Line
	5600 3300 6650 3300
Wire Wire Line
	6550 3400 6650 3400
Wire Wire Line
	6550 3200 6550 3400
Wire Wire Line
	6650 3200 6550 3200
Wire Wire Line
	6650 3500 6550 3500
Wire Wire Line
	6650 3100 6550 3100
Text GLabel 6550 3100 0    39   Input ~ 0
U_POSITION1
Text GLabel 6550 3500 0    39   Input ~ 0
U_POSITION3
$EndSCHEMATC
